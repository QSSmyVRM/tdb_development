<!--ZD 100147 Start-->
<!--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147  ZD 100866 End-->
<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" Inherits="en_UserMenuController" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>UserMenuController</title>
	<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
    <script type="text/javascript" src="extract.js"></script>

    <script type="text/javascript">        // FB 2790
        // ZD 100263
        if (document.addEventListener != null) {
            document.addEventListener("contextmenu", function(e) {
                e.preventDefault();
            }, false);
        }
    
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>

    <script type="text/javascript">

var mmm_num, mma_num, mms_num=new Array(8);
var mmm_int, mma_int, mms_int=new Array(8);
var mms_tn = 19; // ZD 101233 //ZD 102532
var needblock = false;

var orgfrm = ((queryField("n") == "1") ? "opener.document." : "parent.document.");
function SubmenuLayout(cb,idno)
{

	var t = (cb.checked) ? true : false;
	tmpary = (getIdScope (cb.name)).split(",");
	switch (cb.name) {
		case "ma":
			num = getMenuNumber (cb.name);
			for (var i = 1; i <= num; i++) {
				eval("document.frmUsermenucontroller." + cb.name + "_" + i).checked = (!cb.checked)? false : eval("document.frmUsermenucontroller." + cb.name + "_" + i).checked;
			}
		break;
        
		case "m2": // ZD 101233
		case "m2_4":
		case "m5":
		    for (i=parseInt(tmpary[0],10); i<=parseInt(tmpary[1],10); i++) 
		    {
	            for (j=1; j<=mms_num[i]; j++) {
	            if(eval("document.frmUsermenucontroller.m"+idlayout(i)+"_"+j)!= null && idlayout(i).toString() == cb.name.replace('m','')) // ZD 101233
	                eval("document.frmUsermenucontroller.m"+idlayout(i)+"_"+j).checked = (!cb.checked)? false : eval("document.frmUsermenucontroller.m"+idlayout(i)+"_"+j).checked;
	            }
	            tmp="m"+idlayout(i);
	            idShow(tmp, mms_num[i]);
            }
         break;   
		default:
		    for (j=1; j<=mms_num[idno]; j++) 
		        eval("document.frmUsermenucontroller."+cb.name+"_"+j).checked = (!cb.checked)? false : eval("document.frmUsermenucontroller."+cb.name+"_"+j).checked;
	        
	        tmp=cb.name;
	        idShow(tmp, mms_num[idno]);
		break;
	}
	document.frmUsermenucontroller.ma_5.checked = true;
	document.frmUsermenucontroller.ma_5.disabled = true;
	setFoodRoom ();

	switch (cb.name) {
		case "m1":
			chklink(1);
		break;
		case "m2":
		case "m3":
		case "m4":
		case "m5":
		case "m6":
		case "m7": //FB 1779
		case "m8": //FB 1779
			chklink(0);
		break;
	}

	if (queryField("n") == "0") {
		getUserMenuOpinion();
	}
}

function AnalyseMenuStr(mstr)
{

	mm_ary = mstr.split("-");
	if (mm_ary.length!=3) {
		alert (UserMenu);
		var isContinue = confirm(UserErrmsg);
		if (isContinue) {
			ShortkeyLayout(0) 
		} else {
			blockanyinput();
		}
		return;
	}
	mmm_str = mm_ary[0];	mms_str = mm_ary[1];	mma_str = mm_ary[2];

	mmm_ary = mmm_str.split("*");
	if (mmm_ary.length!=2) {
		alert (UserMenu)
		var isContinue = confirm(UserErrmsg)
		if (isContinue) {
			ShortkeyLayout(0) 
		} else {
			blockanyinput();
		}
		return;
	}
	mmm_num = parseInt(mmm_ary[0], 10);	
	mmm_int = parseInt(mmm_ary[1], 10);

	mms_ary = mms_str.split("+");

	for (i=0; i<mms_ary.length; i++) {
		mms_ary[i] = mms_ary[i].split("*");
		if (mms_ary[i].length!=2) {
			alert (UserMenu)
			var isContinue = confirm("Do you want to ignore the error and continue?\nIf click OK, we will help you fix.\nIf click cancel, we will leave it as it be.")
			if (isContinue) {
				ShortkeyLayout(0) 
			} else {
				blockanyinput();
			}
			return;
		}
		
		mms_num[(i+1)] = parseInt(mms_ary[i][0], 10);	
		mms_int[(i+1)] = parseInt(mms_ary[i][1], 10);
	}

	mma_ary = mma_str.split("*");
	if (mma_ary.length!=2) {
		alert (UserMenu)
		var isContinue = confirm("Do you want to ignore the error and continue?\nIf click OK, we will try to help you fix.\nIf click cancel, we will leave it as it be.")
		if (isContinue) {
			ShortkeyLayout(0) 
		} else {
			blockanyinput();
		}
		return;
	}
	mma_num = parseInt(mma_ary[0], 10);	
	mma_int = parseInt(mma_ary[1], 10);
}
function getMenuNumber (strMname) {
	trgname = strMname.substring(1, strMname.length);
	if ( isNaN(trgname) ) {
		menunum = eval("m" + strMname + "_num");
	} else {
		trgname = parseInt(trgname, 10);
		menunum = mms_num[trgname];
	}
	
	return (menunum);
}
function idShow(srcidstr, rstidnum)
{
	var cb = eval("document.frmUsermenucontroller." + srcidstr);
	var t = (cb.checked) ? '' : 'none';
	for (var i = 1; i <= rstidnum; i++) {		
		if(document.getElementById('id_'+srcidstr+'_'+i) != null)		
		document.getElementById('id_'+srcidstr+'_'+i).style.display = t;
		
		if (!cb.checked) {
		if(document.getElementById(cb.name+'_'+i)!=null)
			document.getElementById(cb.name+'_'+i).checked = false;
		}
	}
}

function getIdScope(idstr)
{
	switch (idstr) {
		case "m1":
			startidno = 0;			endidno = -1;
		break;
		case "m2":
			startidno = 1; 			endidno = 8; //8 // ZD 101233
		break;
        case "m2_4":
			startidno = 1; 			endidno = 8; //7
		break;        
		case "m3":
			startidno = 1; 			endidno = 2;
		break;
		case "m4":
			startidno = 1; 			endidno = 4;
		break;
		case "m5":
			startidno = 1;			endidno = 17; //8
		break;
		case "m6":
			startidno = 1;			endidno = 1;
		break;
		//FB 1779
		case "m7":
			startidno = 0;			endidno = -1;
		break;
		case "m8":
			startidno = 0;			endidno = -1;
		break;
		//FB 1779
		case "ma":
			startidno = 1; 			endidno = 6;
		break;
		default:
			startidno = 0; 			endidno = -1;
		break;		
	}
	return(startidno+","+endidno);
}

function SubsubmenuLayout(cb, idno)
{

   	var t = (cb.checked) ? true : false;
	for (j=1; j<=mms_num[idno]; j++) 
		document.getElementById('m'+idlayout(idno)+'_'+j).checked = (!cb.checked)? false : document.getElementById('m'+idlayout(idno)+'_'+j).checked;
	tmp="m"+idlayout(idno);
	idShow(tmp, mms_num[idno]);
	maHide ();
	
	switch (idno) {
		case 4:
		case 18:
			chklink(0);
			break;
	}
	
	if (queryField("n") == "0") {
		getUserMenuOpinion();
	}
}

function blockanyinput()
{
	for (var i=0; i<document.forms[0].length; i++)
		document.frmUsermenucontroller.elements[i].disabled = true;
	needblock = true;
}
function init ()
{
		needblock = false;
		if ( ( (eval(orgfrm + queryField("f") + ".Active").value) == "1" ) || 
		     ( (eval(orgfrm + queryField("f") + ".Locked").value) == "1" ) ) 
		{
			blockanyinput();
			
			//---
			mstr = eval(orgfrm + queryField("f") + ".MenuMask").value;
			mm_ary = mstr.split("-");
			if (mm_ary.length==3) {
				mmm_str = mm_ary[0];
				mmm_ary = mmm_str.split("*");
				if (mmm_ary.length==2) {
					mmm_num = parseInt(mmm_ary[0], 10);	
					mmm_int = parseInt(mmm_ary[1], 10);
				}
			}
	    } else {
	        if(document.forms[0].length > 0)
    		    for (var i=0; i<document.forms[0].length; i++)
	    		    document.frmUsermenucontroller.elements[i].disabled = false;
        }
   		setUserMenuOpinion(eval(orgfrm + queryField("f") + ".MenuMask").value);
   		//document.getElementById("m1").disabled = true;
		//FB 1779 start
		 document.getElementById("m7").disabled = true;
		 document.getElementById("m8").disabled = true;
		//FB 1779 end
		 document.getElementById("m6").disabled = true; //FB 1662

        document.getElementById("m2_4_8").disabled = true; //ZD 102532
        document.getElementById("m2_4_8_1").disabled = true;
        document.getElementById("m2_4_8_2").disabled = true;

}

function setUserMenuOpinion(mm_str)
{

    AnalyseMenuStr(mm_str);
	for (i=1; i<=mmm_num; i++)
	{
	    if(eval("document.frmUsermenucontroller.m" + i) != null)//ZD 100381
		    eval("document.frmUsermenucontroller.m" + i).checked = (mmm_int & (1<<(mmm_num-i))) ? true : false;
	}	
    //FB 1779 start
    //ZD 101388 Starts
    //document.getElementById("m1").checked = true;
    /*if(eval(document.getElementById("m7").checked) && eval(document.getElementById("m8").checked))
        document.getElementById("m1").checked = false;
        
    if(eval(document.getElementById("m7").checked) && eval(document.getElementById("m2").checked))
        document.getElementById("m1").checked = false;*/
    //ZD 101388 End
    //FB 1779 End
    
	for (i=1; i<=mms_tn; i++) 
	{
		for (j=1; j<=mms_num[i]; j++) 
		{
		    if(eval("document.frmUsermenucontroller.m"+idlayout(i)+"_"+j) !=null)//ZD 100381
			    eval("document.frmUsermenucontroller.m"+idlayout(i)+"_"+j).checked = (mms_int[i] & (1<<(mms_num[i]-j) )) ? true : false;
		}
		tmp="m"+idlayout(i);
		idShow(tmp, mms_num[i]);
	}
	document.frmUsermenucontroller.ma.checked = true;
	for (i=1; i<=mma_num; i++) {
	    if(eval("document.frmUsermenucontroller.ma_"+i) != null)//ZD 100381
		    eval("document.frmUsermenucontroller.ma_"+i).checked = (mma_int & (1<<(mma_num-i))) ? true : false;
	}

	SubmenuLayout(document.frmUsermenucontroller.ma);

	maHide ();

}
function maHide ()
{

	//document.getElementById('id_ma_1').style.display = "none"; // ZD 101233
	document.getElementById('id_ma_2').style.display = "none";
	document.getElementById('id_ma_3').style.display = "none"; // ZD 101233
	//document.getElementById('id_ma_4').style.display = "none";
	document.getElementById('id_ma_6').style.display = "none";
}

function idlayout(idno)
{
	switch (idno) {
		case 1:
		    idstr = "2";	    break;//4 // ZD 101233
		case 2:
		    idstr = "2_4";	    break;//7
		case 3:
		    idstr = "2_4_1";	break;//3
		case 4:
		    idstr = "2_4_2";	break;//4
		case 5:
		    idstr = "2_4_3";	break;//5
		case 6:
		    idstr = "2_4_4";	break;//5
		case 7:
		    idstr = "2_4_5";	break;//5
		case 8:
		    idstr = "2_4_6";	break;//2 //ZD 102532
        case 9:
		    idstr = "2_4_8";	break;//2
		case 10:
		    idstr = "3";	    break;//2
		case 11:
			idstr = "4";	    break;//4
		case 12:
			idstr = "5";	    break;//8
		case 13:
			idstr = "5_1";	    break;//3
		case 14:
			idstr = "5_2";	    break;//2
		case 15:
			idstr = "5_3";	    break;//8
		case 16:
			idstr = "5_6";	    break;//2 //FB 2593 //FB 2885
		case 17:
			idstr = "5_7";	    break;//2 //FB 2593 //FB 2885
		case 18:
			idstr = "5_8";	    break;//2 //FB 2593 //FB 2885
		case 19:
			idstr = "6";	    break;//1	
		default:
			alert(UserMenuError); break;
	}
	
	return (idstr);
}
function setFoodRoom()
{
	showFood = parseInt("<%=Session["foodModule"]%>", 10);
	showRoom = parseInt("<%=Session["roomModule"]%>", 10);
	showhk = parseInt("<%=Session["hkModule"]%>", 10);

    //FB 2885 Starts
	if (!showFood) {
		document.getElementById('id_m5_7').style.display = "none";
		document.getElementById('id_m5_7_1').style.display = "none";
		document.getElementById('id_m5_7_2').style.display = "none";
	}

	if (!showRoom) {
		document.getElementById('id_m5_6').style.display = "none";
		document.getElementById('id_m5_6_1').style.display = "none";
		document.getElementById('id_m5_6_2').style.display = "none";
	}
	
	if (!showhk) {
		document.getElementById('id_m5_8').style.display = "none";
		document.getElementById('id_m5_8_1').style.display = "none";
		document.getElementById('id_m5_8_2').style.display = "none";
	}
	//FB 2885 Ends
}

function getUserMenuOpinion()
{	
	mm_str = "";
	mmm_int = 0;	mma_int = 0;
	for (i=1; i<=mms_tn; i++) {
		mms_int[i] = 0;
	}

	for (i=1; i<=mmm_num; i++) {
	if(document.getElementById('m'+i) != null)//ZD 100381
		mmm_int = (document.getElementById('m'+i).checked) ? ((mmm_int+1)<<1)  : (mmm_int<<1);
	}

	mmm_int = mmm_int>>>1;
	
	for (i=1; i<=mms_tn; i++) {
		for (j=1; j<=mms_num[i]; j++) {
		if(document.getElementById('m'+idlayout(i)+'_'+j) !=null)
			mms_int[i] = (document.getElementById('m'+idlayout(i)+'_'+j).checked) ? ((mms_int[i]+1)<<1)  : (mms_int[i]<<1);
		}
		mms_int[i] = mms_int[i]>>>1;
	}
	
	for (i=1; i<=mma_num; i++) {
	if(document.getElementById('ma_'+i) != null)//ZD 100381
		mma_int = (document.getElementById('ma_'+i).checked) ? ((mma_int+1)<<1)  : (mma_int<<1);
	}
	mma_int = mma_int>>>1;
	
	mm_str = mmm_num + "*" + mmm_int + "-"
	for (i=1; i<=mms_tn; i++) {
		if (i==mms_tn) {
			mm_str += mms_num[i] + "*" + mms_int[i]
		} else {
			mm_str += mms_num[i] + "*" + mms_int[i] + "+"
		}
	}
	mm_str += "-" + mma_num + "*" + mma_int


	eval(orgfrm + queryField("f") + ".MenuMask").value = mm_str;
	
	if (eval(orgfrm + queryField("f") + ".sapMValue"))
		eval(orgfrm + queryField("f") + ".sapMValue").value = document.getElementById("m5_5").checked;
	
	if (queryField("f") == "frmManageusertemplate2") {
		eval(orgfrm + queryField("f") + ".RoleID")[0].selected = true;
	}
	
	if (queryField("n") == "0") {
		parent.save() ;
	}
	
	
	window.close ();
}
function chklink(icn)
{
	var s=new Array(6), t=new Array(6);
	if (queryField("n") == "0") {
		getUserMenuOpinion();
	}
}


function chkcheckbox()
{
	if (queryField("n") == "0") {
		getUserMenuOpinion();
	}
}

function deSelect(cb, cid)
{
    if (cb.checked)
        document.getElementById(cid).checked = false;
}

function fnSetRelevantStatus(chk, elemId)
{
    if(chk == true)
    {
        document.getElementById(elemId).checked = true;
    }
    else
    {
        document.getElementById(elemId).checked = false;
    }
}

function fnHoldRelevantStatus(obj, contId)
{
    if(document.getElementById(contId).checked == true)
    {
        obj.checked = true;
    }
}

function fnVerifyClone(cloneId)
{
    if(document.getElementById("m3_2").checked == false)
    {
        document.getElementById(cloneId).checked = false;
        alert('Please select "Advanced Form" to select "Clone" option');//ZD 102054
    }
}

function fnResetClone()
{
    document.getElementById("m2_4_3_5").checked = false;
    document.getElementById("m2_4_4_5").checked = false;
    document.getElementById("m2_4_5_5").checked = false;
}

    </script>

    <!-- script finished -->
    <%--CONTENT GOES HERE --%>
</head>
<body style="margin-bottom: 0; margin-left: 0; margin-right: 0; margin-top: 0;">
    <form id="frmUsermenucontroller" method="POST" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div style="align: left">
        <table border="0" cellpadding="2" width="350">
            <tr>
                <td style="height: 0; width: 20">
                </td>
                <td style="height: 0; width: 30">
                </td>
                <td style="height: 0; width: 300">
                </td>
            </tr>
            <tr id="id_m1">
                <td colspan="3">
                    <asp:CheckBox ID="m1" onclick="javascript:SubmenuLayout(this);" runat="server" />
                    <!-- onclick="javascript:this.='true';" //FB 1779-->
                    <font class="blackblodtext "><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Home%>" runat="server"></asp:Literal></font><%--FB 2579 Start--%>
                </td>
            </tr>
            <tr id="id_m2">
                <td colspan="3">
                    <asp:CheckBox ID="m2" onclick="javascript:SubmenuLayout(this,1);" runat="server" />
                    <!-- onclick="javascript:this.='true';" -->
                    <font class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Conferences%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m2_1">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="m2_1" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Calender%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m2_2">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="m2_2" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_CallMonitor%>" runat="server"></asp:Literal></font>
                </td>
            </tr>

            <%-- ZD 101233 Start --%>
            <tr id="id_m2_3">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m2_3" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, DashBoard%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4" onclick="javascript:SubmenuLayout(this,2);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, RoomTree_List%>" runat="server"/></font>
                </td>
            </tr>

            <tr id="id_m2_4_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_1" onclick="javascript:SubsubmenuLayout(this,3);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, OngoingConferences%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_1_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_1_1" onclick="javascript:fnHoldRelevantStatus(this,'m2_4_1_2');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, View%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_1_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_1_2" onclick="javascript:fnSetRelevantStatus(this.checked,'m2_4_1_1');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, ConferenceList_btnManage%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_1_3">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_1_3" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, ExtendEndTime%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_1_4">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_1_4" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ConferenceList_btnMCUDetails%>" runat="server"/></font>
                </td>
            </tr>

            <tr id="id_m2_4_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_2" onclick="javascript:SubsubmenuLayout(this,4);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, OnMCUConferences%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_2_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_2_1" onclick="javascript:fnHoldRelevantStatus(this,'m2_4_2_2');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, View%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_2_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_2_2" onclick="javascript:fnSetRelevantStatus(this.checked,'m2_4_2_1');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ConferenceList_btnManage%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_2_3">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_2_3" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ConferenceList_btnMCUDetails%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_2_4">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_2_4" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, ConferenceList_btnDelete%>" runat="server"/></font>
                </td>
            </tr>

            <tr id="id_m2_4_3">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_3" onclick="javascript:SubsubmenuLayout(this,5);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, Reservations%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_3_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_3_1" onclick="javascript:fnHoldRelevantStatus(this,'m2_4_3_4');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, View%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_3_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_3_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, RoomSearch_Editroom%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_3_3">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_3_3" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal16" Text="<%$ Resources:WebResources, ConferenceList_btnDelete%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_3_4">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_3_4" onclick="javascript:fnSetRelevantStatus(this.checked,'m2_4_3_1');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal17" Text="<%$ Resources:WebResources, ConferenceList_btnManage%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_3_5">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_3_5" onclick="javascript:fnVerifyClone(this.id);chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal18" Text="<%$ Resources:WebResources, ConferenceList_btnClone%>" runat="server"/></font>
                </td>
            </tr>
           <%--ZD 102532--%>
            <tr id="id_m2_4_4">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_4" onclick="javascript:SubsubmenuLayout(this,6);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal19" Text="<%$ Resources:WebResources, WaitListConferences%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_4_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_4_1" onclick="javascript:fnHoldRelevantStatus(this,'m2_4_4_4');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal20" Text="<%$ Resources:WebResources, View%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_4_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_4_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal21" Text="<%$ Resources:WebResources, RoomSearch_Editroom%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_4_3">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_4_3" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal22" Text="<%$ Resources:WebResources, ConferenceList_btnDelete%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_4_4">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_4_4" onclick="javascript:fnSetRelevantStatus(this.checked,'m2_4_4_1');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal23" Text="<%$ Resources:WebResources, ConferenceList_btnManage%>" runat="server"/></font>
                </td>
            </tr>

             <tr id="id_m2_4_5">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_5" onclick="javascript:SubsubmenuLayout(this,7);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal39" Text="<%$ Resources:WebResources, PersonalCalendar_PublicConferen%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_5_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_5_1" onclick="javascript:fnHoldRelevantStatus(this,'m2_4_5_4');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal40" Text="<%$ Resources:WebResources, View%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_5_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_5_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal41" Text="<%$ Resources:WebResources, RoomSearch_Editroom%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_5_3">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_5_3" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal42" Text="<%$ Resources:WebResources, ConferenceList_btnDelete%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_5_4">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_5_4" onclick="javascript:fnSetRelevantStatus(this.checked,'m2_4_5_1');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal43" Text="<%$ Resources:WebResources, ConferenceList_btnManage%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_5_5">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_5_5" onclick="javascript:fnVerifyClone(this.id);chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal44" Text="<%$ Resources:WebResources, ConferenceList_btnClone%>" runat="server"/></font>
                </td>
            </tr>


            <tr id="id_m2_4_6">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_6" onclick="javascript:SubsubmenuLayout(this,8);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal25" Text="<%$ Resources:WebResources, PendingConferences%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_6_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_6_1" onclick="javascript:fnHoldRelevantStatus(this,'m2_4_6_4');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal26" Text="<%$ Resources:WebResources, View%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_6_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_6_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal27" Text="<%$ Resources:WebResources, RoomSearch_Editroom%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_6_3">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_6_3" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal28" Text="<%$ Resources:WebResources, ConferenceList_btnDelete%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_6_4">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_6_4" onclick="javascript:fnSetRelevantStatus(this.checked,'m2_4_6_1');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal29" Text="<%$ Resources:WebResources, ConferenceList_btnManage%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_6_5">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_6_5" onclick="javascript:fnVerifyClone(this.id);chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal30" Text="<%$ Resources:WebResources, ConferenceList_btnClone%>" runat="server"/></font>
                </td>
            </tr>

            <tr id="id_m2_4_7">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_7" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal31" Text="<%$ Resources:WebResources, ApprovalPending%>" runat="server"/></font>
                </td>
            </tr>

            <tr id="id_m2_4_8">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_8" onclick="javascript:SubsubmenuLayout(this,9);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal32" Text="<%$ Resources:WebResources, ConferencesSupport%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_8_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_8_1" onclick="javascript:fnHoldRelevantStatus(this,'m2_4_8_2');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal33" Text="<%$ Resources:WebResources, View%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m2_4_8_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m2_4_8_2" onclick="javascript:fnSetRelevantStatus(this.checked,'m2_4_8_1');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal34" Text="<%$ Resources:WebResources, ConferenceList_btnManage%>" runat="server"/></font>
                </td>
            </tr>
            <%-- ZD 101233 End --%>
            
            <tr id="id_m3">
                <td colspan="3">
                    <asp:CheckBox ID="m3" onclick="javascript:SubmenuLayout(this,10);" runat="server" />
                    <font class="blackblodtext">
                        <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                          {%>New Hearing<%}
                          else
                          { %><asp:Literal Text="<%$ Resources:WebResources, NewConference%>" runat="server"></asp:Literal><%}%></span>
                        <%--added for FB 1428 Start--%></font>
                </td>
            </tr>

            <%-- ZD 101233 Start --%>
            <tr id="id_m3_1">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m3_1" onclick="javascript:fnHoldRelevantStatus(this,'m3_2');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal35" Text="<%$ Resources:WebResources, ExpressInterface%>" runat="server"/></font>
                </td>
            </tr>
            <tr id="id_m3_2">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m3_2" onclick="javascript:fnResetClone();fnSetRelevantStatus(this.checked,'m3_1');chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal36" Text="<%$ Resources:WebResources, NormalInterface%>" runat="server"/></font>
                </td>
            </tr>
            <%-- ZD 101233 End --%>

            <tr id="id_m4">
                <td colspan="3">
                    <asp:CheckBox ID="m4" onclick="javascript:SubmenuLayout(this,11);" runat="server" />
                    <font class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_MySettings%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m4_1">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="m4_1" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AccountSettings%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m4_2">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="m4_2" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Reports%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m4_3">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="m4_3" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Templates%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m4_4">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="m4_4" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Groups%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5">
                <td colspan="3">
                    <asp:CheckBox ID="m5" onclick="javascript:SubmenuLayout(this,12);" runat="server" />
                    <font class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Administration%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_1">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_1" onclick="javascript:SubsubmenuLayout(this,13);" runat="server" />
                    <font class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Hardware%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_1_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_1_1" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Endpoints%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_1_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_1_2" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Diagnostics%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_1_3">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_1_3" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_MCUs%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_1_4">
                <%--FB 2023--%>
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_1_4" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_AudioBridges%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_1_5">
                <%--DD2--%>
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_1_5" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_EM7%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_2">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_2" onclick="javascript:SubsubmenuLayout(this,14);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Locations%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_2_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_2_1" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Rooms%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_2_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_2_2" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Tiers%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_2_3"> <%--ZD 102123--%>
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_2_3" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, FloorPlans%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_3">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_3" onclick="javascript:SubsubmenuLayout(this,15);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Users%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_3_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_3_1" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_ActiveUsers%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_3_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_3_2" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_BulkTool%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_3_3">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_3_3" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Departments%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_3_4">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_3_4" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Guests%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_3_5">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_3_5" onclick="javascript:chkcheckbox();" runat="server" ForeColor="green" />
                </td>
            </tr>
            <tr id="id_m5_3_6">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_3_6" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_LDAPDirectory%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_3_7">
                <td colspan="3"> <%--ZD 101525--%>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_3_7" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal38" Text="<%$ Resources:WebResources, LDAPGroups%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_3_8">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_3_8" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Roles%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_3_9">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_3_9" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Templates%>" runat="server"></asp:Literal></font>
                    <br />
                    <hr width="100%" />
                </td>
            </tr>
            <tr id="id_m5_4">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="m5_4" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Options%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_5">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="m5_5" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Settings%>" runat="server"></asp:Literal></font>
                    <br />
                    <hr width="100%" />
                </td>
            </tr>
            <tr id="id_m5_6">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_6" onclick="javascript:SubsubmenuLayout(this,16);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Inventory%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_6_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_6_1" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_InventorySets%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_6_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_6_2" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_WorkOrders%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_7">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_7" onclick="javascript:SubsubmenuLayout(this,17);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Catering%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_7_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_7_1" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Menus%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_7_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_7_2" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_WorkOrders%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_8">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_8" onclick="javascript:SubsubmenuLayout(this,18);" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Facility%>" runat="server"></asp:Literal></font>
                    <%--FB 2570--%>
                </td>
            </tr>
            <tr id="id_m5_8_1">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_8_1" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, FacilityServices%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m5_8_2">
                <td colspan="3">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox id="m5_8_2" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_WorkOrders%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <%--FB 2885 Ends --%>
            <tr id="id_m6">
                <td colspan="3">
                    <asp:CheckBox ID="m6" onclick="javascript:SubmenuLayout(this,19);" runat="server" />
                    <font class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Site%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m6_1">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="m6_1" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Settings%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr style="width: 10" />
                </td>
            </tr>
            <%--//FB 1779 EXPRESS CONFERENCE Starts--%>
            <tr id="id_m7" style="display:none;"> <%--ZD 101388--%>
                <td colspan="3">
                    <asp:CheckBox id="m7" onclick="javascript:SubmenuLayout(this);" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_ScheduleaCall%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr id="id_m8" style="display:none;"> <%--ZD 101388--%>
                <td colspan="3">
                    <asp:CheckBox id="m8" onclick="javascript:SubmenuLayout(this);" runat="server"></asp:CheckBox>
                    <font size="2" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_ViewEditReserv%>" runat="server"></asp:Literal></font>
                </td>
            </tr>
            <tr style="display:none;"> <%--ZD 101388--%>
                <td colspan="3"> 
                    <hr style="width: 10" />
                </td>
            </tr>
            <%-- //FB 1779EXPRESS CONFERENCE Ends--%>
            <tr>
                <td colspan="3">
                    <asp:CheckBox id="ma" onclick="javascript:SubmenuLayout(this);" runat="server"></asp:CheckBox>
                    <font color="green" size="2"><i><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Others%>" runat="server"></asp:Literal></i></font>&nbsp;<%--FB 2579--%>
                </td>
            </tr>
            <tr id="id_ma_1">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="ma_1" checked="true" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" color="green"><i><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Search%>" runat="server"></asp:Literal></i></font>
                </td>
            </tr>
            <tr id="id_ma_2">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="ma_2" checked="true" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" color="green"><i><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_AboutUs%>" runat="server"></asp:Literal></i></font>
                </td>
            </tr>
            <tr id="id_ma_3">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="ma_3" checked="true" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" color="green"><i><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Feedback%>" runat="server"></asp:Literal></i></font>
                </td>
            </tr>
            <tr id="id_ma_4">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="ma_4" checked="true" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" color="green"><i><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Help%>" runat="server"></asp:Literal></i></font>
                </td>
            </tr>
            <tr id="id_ma_5">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="ma_5" checked="true" enabled="false" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" color="green"><i><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Logoff%>" runat="server"></asp:Literal></i></font>
                </td>
            </tr>
            <tr id="id_ma_6">
                <td colspan="3">
                    &nbsp;&nbsp;
                    <asp:CheckBox id="ma_6" checked="true" onclick="javascript:chkcheckbox();" runat="server"></asp:CheckBox>
                    <font size="2" color="green"><i><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_CompanyLink%>" runat="server"></asp:Literal></i></font>
                </td>
            </tr>

            <tr id="id_ma_7" > <%-- ZD 101233 --%>
                <td colspan="3">
                    <asp:CheckBox ID="ma_7" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext"><asp:Literal ID="Literal37" Text="<%$ Resources:WebResources, AdminRights%>" runat="server"/></font>
                </td>
            </tr>

        </table>
    </div>
    <br>
    <center>
        <table border="0" cellpadding="0" width="98%" id="ButtonTable" runat="server" style="display: none">
            <tr>
                <td align="center">
                    <asp:Button id="UsermenucontrolSubmit1" text="<%$ Resources:WebResources, UserMenuController_UsermenucontrolSubmit1%>" cssclass="altShort2BlueButtonFormat" runat="server"></asp:Button>
                </td>
                <td align="center">
                    <asp:Button id="UsermenucontrolSubmit2" text="<%$ Resources:WebResources, UserMenuController_UsermenucontrolSubmit2%>" cssclass="altShort2BlueButtonFormat" runat="server"></asp:Button>
                </td>
            </tr>
        </table>
    </center>
    </form>

    <script type="text/javascript">
        init();
        document.getElementById("m1").checked = true; // ZD 101388
        document.getElementById("m2_4_8").disabled = true; //ZD 102532
        document.getElementById("m2_4_8_1").disabled = true;
        document.getElementById("m2_4_8_2").disabled = true;
    </script>

</body>
</html>

<script type="text/javascript" src="inc/softedge.js"></script>

