<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ViewCustomAttributes"%>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<%--Edited for FF--%>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
{%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%}
else {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%} %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">

<script>
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
function FnCancel() {
    DataLoading(1); //ZD 100176
	window.location.replace('organisationsettings.aspx');
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" id="Head1">
<%--FB 1975 - Start--%>
<script>
/*
function suppressBackspace(evt) 
{
     evt = evt || window.event;
     var target = evt.target || evt.srcElement; 
     if (evt.keyCode == 8 && !/input|textarea/i.test(target.nodeName)) 
     { 
        window.location.replace('OrganisationSettings.aspx');        
        return false;
     } 
} 
document.onkeydown = suppressBackspace;
document.onkeypress = suppressBackspace; 
*/
//FB 2045 - Start
function fnconfirmdel(status)
{
    if(status == true)
        return false;

    if (confirm(RSdeleteCustomOption))
    {
        return true;
    }
    else
    {
        return false;
    }
}
//FB 2045 - End
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100176 End
document.onkeydown = function(evt) {
    evt = evt || window.event;
    var keyCode = evt.keyCode;
    if (keyCode == 8) {
        if (document.getElementById("btnCancel") != null) { // backspace
            var str = document.activeElement.type;
            if (!(str == "text" || str == "textarea" || str == "password")) {
                document.getElementById("btnCancel").click();
                return false;
            }
        }
        if (document.getElementById("btnGoBack") != null) { // backspace
            var str = document.activeElement.type;
            if (!(str == "text" || str == "textarea" || str == "password")) {
                document.getElementById("btnGoBack").click();
                return false;
            }
        }
    }
    fnOnKeyDown(evt);
};
</script>
<%--FB 1975 - End--%>
    <title>View Custom Options</title>
</head>
<body >
    <form id="frmCustomAttribute" runat="server" method="post" >
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
     <input type="hidden" id="helpPage" value="73"/>
     <input type="hidden" id="HdnCustOptID" runat="server" value="<%=customAttrID%>" />
        <table width="100%">
            <tr>
                <td align="center" colspan="2">
                    <h3><asp:Literal Text="<%$ Resources:WebResources, ViewCustomAttributes_OrganizationCu%>" runat="server"></asp:Literal><!-- FB 2570 -->
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 1168px" colspan="2">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                        <img border='0' src='image/wait1.gif' alt="<asp:Literal Text='<%$ Resources:WebResources, Loading%>' runat='server' />" />
                    </div><%--ZD 100678 End--%>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2"> <%--FB 1779 ItemDatabound added--%>
                    <asp:DataGrid ID="dgCustomAttribute" runat="server" AutoGenerateColumns="False" CellPadding="2" GridLines="None" AllowSorting="true" 
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="False" Width="85%" OnItemCreated="BindRowsDeleteMessage"
                         OnItemDataBound="BindRowsToGrid" OnDeleteCommand="DeleteCustomGrid" OnEditCommand="EditCustomGrid" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%><%--FB 1779--%>
                        <SelectedItemStyle  CssClass="tableBody"/>
                          <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody"/>
                        <Columns>
                            <asp:BoundColumn DataField="CustomAttributeID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, CustomAttributeID%>" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RowUID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, SNo%>"></asp:BoundColumn>
                            <asp:BoundColumn ItemStyle-Width="10%" DataField="CreateType" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, CreateType%>"></asp:BoundColumn><%--FB 1779--%>
                            <asp:BoundColumn ItemStyle-Width="20%" DataField="Title" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, DisplayTitle%>" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn ItemStyle-Width="25%" DataField="Description" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Description%>" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn ItemStyle-Width="15%" DataField="Type" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ConferenceList_Type%>" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn DataField="CAStatus" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ManageCustomAttribute_EnableStatus%>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Mandatory" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, MandatoryStatus%>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="IncludeInEmail" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, mainadministrator_IncludeinEmai%>"></asp:BoundColumn>
                            
                            <asp:TemplateColumn Visible="false" HeaderText="<%$ Resources:WebResources, OptionValues%>" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader">
                                <ItemTemplate>
                                <table>
                                <tr>
                                    <td>
                                        <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ViewCustomAttributes_ViewOption%>" id="ViewOption" commandname="ViewOption"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                <td colspan="8">
                                  <asp:Table runat="server" ID="tblCustomOptions" Visible="true" BorderColor="black" Width="100%" CellPadding="0" CellSpacing="0" BorderWidth="1" BorderStyle="Inset">
                                    <asp:TableRow>
                                        <asp:TableCell>                                                
                                            <asp:DataGrid ID="dgCustomOptions" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="Small"
                                             Width="100%" BorderColor="blue" BorderStyle="Solid" BorderWidth="0px" OnItemCreated="BindRowsDeleteMessage" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                                                <HeaderStyle CssClass="tableHeader" Height ="30"/>
                                                <Columns>
                                                    <asp:BoundColumn DataField="OptionID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, OptionID%>" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DisplayCaption" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, DisplayTitle%>"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="HelpText" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, HelpText%>"></asp:BoundColumn>
                                                   </Columns>
                                              </asp:DataGrid>
                                             </asp:TableCell>
                                       </asp:TableRow>
                                    </asp:Table>
                                   </td>               
                                </tr>
                                </table>
                               </ItemTemplate>
                             </asp:TemplateColumn>
                              <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, InventoryManagement_Actions%>" HeaderStyle-HorizontalAlign="center">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ViewCustomAttributes_btnEdit%>" id="btnEdit" commandname="Edit" onclientclick="javascript:DataLoading(1);"></asp:LinkButton><%--ZD 100176--%> 
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ViewCustomAttributes_btnDelete%>" id="btnDelete" commandname="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" id="tblNoCustomAttribute" visible="false" width="90%">
                        <asp:TableRow cssclass="lblError">
                            <asp:TableCell cssclass="lblError" horizontalalign="center">
                               <font class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ViewCustomAttributes_Nocustomoptio%>" runat="server"></asp:Literal></font>  
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <asp:Button ID="CustomTrigger" runat="server" style="display:none;"  />
                    <ajax:ModalPopupExtender ID="CustomPopUp" runat="server" TargetControlID="CustomTrigger"  PopupControlID="PopupCustomPanel"  DropShadow="false" Drag="true" BackgroundCssClass="modalBackground" CancelControlID="ClosePUp" BehaviorID="CustomTrigger"></ajax:ModalPopupExtender> 
                    <asp:Panel ID="PopupCustomPanel" Width="60%" runat="server" HorizontalAlign="Center"  CssClass="treeSelectedNode" Height="600px"  ScrollBars="Auto"> 
                        
                        <table align="center" border="0" cellpadding="3" cellspacing="0" width="95%">
                            <tr><td><br></td></tr>
                            <tr>
                                <td align="right">
                                    <asp:ImageButton ID="btnExcel" OnClick="ExportExcel" src="image/excel.gif"  runat="server" style="vertical-align:middle;" ToolTip="<%$ Resources:WebResources, UtilizationReport_btnExcel%>"/>
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="lblError" id="ConfMessage" runat="server"></td>
                            </tr>
                            <tr>
                                <td>
                                    <br>
                                    <div style="width:auto;overflow:auto;"> <%--FB 1750--%>
                                        <asp:Table ID="ConfListTbl" runat="server" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1" CellPadding="3" CellSpacing="1" Height="445px" HorizontalAlign="Center" >
                                        </asp:Table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Button id="BtnDeleteAll" runat="server" text="<%$ Resources:WebResources, ViewCustomAttributes_BtnDeleteAll%>" onclick="DeleteCustomOptions" cssclass="altShort0BlueButtonFormat" style="width:170px"></asp:Button> <%--FB 2985--%>
                        <asp:Button id="BtnEditCA" runat="server" text="<%$ Resources:WebResources, ViewCustomAttributes_BtnEditCA%>" onclick="EditCustomOptions" cssclass="altShort0BlueButtonFormat" style="width:250px"></asp:Button> <%--FB 2985--%>
                        <input align="middle" type="button" runat="server" style="width:150px" id="ClosePUp" value=" <%$ Resources:WebResources, BridgeDetails_BtnClose%> " class="altShort0BlueButtonFormat" />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align="center">
                   <Button ID="btnCreateCusAtt" runat="server" onserverclick="CreateNewCustomAtt" style="width:280px" OnClick="javascript:DataLoading(1);"><asp:Literal text='<%$ Resources:WebResources, ViewCustomAttributes_btnCreateCusAtt%>' runat='server' /></Button>&nbsp;&nbsp;<%-- FB 2796--%> <%--ZD 100176--%> <%--ZD 100420--%> 
                   <input class="altMedium0BlueButtonFormat" onclick="FnCancel()" type="button" value="<%$ Resources:WebResources, Cancel%>" runat="server" name="btnCancel" id="btnCancel"/>
                </td>
            </tr>
        </table>
    
</form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->