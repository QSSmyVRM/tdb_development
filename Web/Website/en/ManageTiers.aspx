<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_Tiers.Tiers" Buffer="true" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>


<head runat="server">
    <title>Manage Tiers</title>
    <%--//ZD 101244--%>
    <style type="text/css">
        .labelwrap
        {
          word-break :break-all;
        }
    </style>
    <%--ZD 101244 End--%>
    <script type="text/javascript" src="inc/functions.js"></script>
    <script type="text/javascript" >
        //ZD 100604 start
        var img = new Image();
        img.src = "../en/image/wait1.gif";
        //ZD 100604 End
    //ZD 100176 start
		function DataLoading(val) {
		    if (val == "1")
		        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
		    else
		        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
		}
		//ZD 100176 End

        //ZD 101244
		function EnableValidate() {
		    if (document.getElementById("ChkSecure") != null) {
		        if ((document.getElementById("ChkSecure").checked))
		            ValidatorEnable(document.getElementById("reqSecurityemail"), true);
		        else
		            ValidatorEnable(document.getElementById("reqSecurityemail"), false);
		    }
		}
		function EnableValidate1(obj) {
		    var custId = obj.id.split("_");
		    
		    if (obj.checked)
		        ValidatorEnable(document.getElementById(custId[0] + "_" + custId[1] + "_" + "reqSecurityemail1"), true);
		    else
		        ValidatorEnable(document.getElementById(custId[0] + "_" + custId[1] + "_" + "reqSecurityemail1"), false);
		}
		function Checkemail(obj) {
		    var custId = obj.id.split("_");
		    if ((document.getElementById(custId[0] + "_" + custId[1] + "_" + "ChkSecure1") != null) && (document.getElementById(custId[0] + "_" + custId[1] + "_" + "ChkSecure1").checked)) {
		        if (document.getElementById(custId[0] + "_" + custId[1] + "_" + "txtsecurityemail1").value == "") {
		            ValidatorEnable(document.getElementById(custId[0] + "_" + custId[1] + "_" + "reqSecurityemail1"), true);
		            return false;
		        }
		        else
		            ValidatorEnable(document.getElementById("reqSecurityemail"), false);
		    }
		}
		function fnCheckemail(sender, args) {
            var vaild =true;
            var emaillist = document.getElementById('txtsecurityemail').value;
		    var Spilitemail = emaillist.split(';');
            var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		    for (var i = 0; i < Spilitemail.length; i++) {
		        if (Spilitemail[i] = "" || !regex.test(Spilitemail[i])) {
		            document.getElementById('customsecurityemail').style.display = 'block';
		            return args.IsValid = false;
		        }
		        else {
		            document.getElementById('customsecurityemail').style.display = 'none';
                }
		    }
		}

		function fnCheckemail1(sender, args) {
		    var custId = sender.id.split("_");
		    var emaillist = document.getElementById(custId[0] + "_" + custId[1] + "_" + "txtsecurityemail1").value;
		    var vaild = true;
		    var Spilitemail = emaillist.split(';');
		    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		    for (var i = 0; i < Spilitemail.length; i++) {
		        if (Spilitemail[i] = "" || !regex.test(Spilitemail[i])) {
		            document.getElementById(custId[0] + "_" + custId[1] + "_" + "customsecurityemail1").style.display = 'block';
		            args.IsValid = false;
		        }
		        else {
		            document.getElementById(custId[0] + "_" + custId[1] + "_" + "customsecurityemail1").style.display = 'none';
		        }

		    }
		}
		//ZD 101244 End
		</script>
</head>
<body>
    <form id="frmTierManagement" runat="server" method="post" onsubmit="return true;DataLoading(1)"> <%--ZD 100176--%> 
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div> 
      <input type="hidden" id="helpPage" value="65">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
            <div id="dataLoadingDIV" style="display:none" align="center" >
                <img border='0' src='image/wait1.gif'  alt='Loading..' />
            </div> <%--ZD 100678 End--%>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTiers_ExistingTiers%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgTier1s" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnUpdateCommand="UpdateTier1" OnCancelCommand="CancelTier1" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteTier1" OnEditCommand="EditTier1" Width="90%" Visible="true" style="border-collapse:separate" > <%--Edited for FF--%>
                        <%--Window Dressing - Start--%>
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" /><%--Added for Window Dressing --%>                        
                        <%--Window Dressing - End--%>
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Name%>" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="40%"> <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblTier1Name" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtTier1Name" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqTier1Name1" ControlToValidate="txtTier1Name" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" ValidationGroup="Update"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regName" ControlToValidate="txtTier1Name" runat="server" Display="Dynamic" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters48%>" ValidationGroup="Update" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^@#$%&()~',]*$"></asp:RegularExpressionValidator> <%-- fogbugz case 137 for extra junk character validation--%> <%--FB 1888--%><%--ZD 103424--%>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ManageTiers_btnManageTier2%>"><%--ZD 100425--%>
                                <ItemTemplate>
                                    <asp:Button ID="btnManageTier2" runat="server" CssClass="altLongBlueButtonFormat" OnCommand="ManageTier2" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID") + "^" + DataBinder.Eval(Container, "DataItem.Name") %>' Text="<%$ Resources:WebResources, ManageTiers_btnManageTier2%>" CommandName="select"   OnClientClick="DataLoading(1)"/><%--ZD 100176--%> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" ItemStyle-Width="15%">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageTiers_btnEdit%>" id="btnEdit" commandname="Edit" onclientclick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                    &nbsp;<asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageTiers_btnDelete%>" id="btnDelete" commandname="Delete" onclientclick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageTiers_btnUpdate%>" id="btnUpdate" ValidationGroup="Update" commandname="Update" visible="false" onclientclick="Checkemail(this);" ></asp:LinkButton><%--ZD 100176--%> 
                                    &nbsp;<asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageTiers_btnCancel%>" id="btnCancel" commandname="Cancel" visible="false" onclientclick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--ZD 101244 start--%>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Secure%>" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                             <ItemTemplate>
                               <asp:CheckBox ID="ChkSecure1" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Secure").Equals("1") %>' Enabled="false" onclick="javascript:EnableValidate1(this);"/>
                             </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ManageTier_securityemail%>" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="20%" ItemStyle-Wrap="True">
                             <ItemTemplate>
                                <asp:Label ID="lblsecurityemail1s" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Securityemail") %>' Width="200px" CssClass="labelwrap"></asp:Label>
                             </ItemTemplate>
                             <EditItemTemplate>
                                <asp:TextBox ID="txtsecurityemail1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Securityemail") %>'/>
                                <asp:RequiredFieldValidator ID="reqSecurityemail1" Enabled="false" ControlToValidate="txtsecurityemail1" runat="server"  ValidationGroup="Update"
                                ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                <asp:CustomValidator id="customsecurityemail1" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" Display="Dynamic" ValidationGroup="Update"
                                    ControlToValidate="txtsecurityemail1" runat="server"  ClientValidationFunction="fnCheckemail1"></asp:CustomValidator>
                             </EditItemTemplate>
                             <FooterTemplate>
                                <%--Window Dressing--%><div style="text-align :right">
                                    <span class="blackblodtext" ><asp:Literal Text="<%$ Resources:WebResources, ManageTiers_TotalTiers%>" runat="server"></asp:Literal></span> <asp:Label id="lblTotalRecords" runat="server" text=""></asp:Label><%--FB 2579--%>
                                    </div>
                                </FooterTemplate>
                            </asp:TemplateColumn><%--ZD 101244 End--%>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoTier1s" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No tier1s found.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <%--<SPAN class=subtitleblueblodtext></SPAN>--%><%--Commented for FB 2094--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trTiers" runat="server" ><td><table align="center" border="0" width="100%"><%--ZD 101244--%>
            <tr id="trNew" runat="server"> <%--FB 2670--%>
            <td style="width:30%"></td>
                <%--Window Dressing--%>
                    <td class="blackblodtext" style="width:13%" valign="top" nowrap="nowrap"><asp:Literal Text="<%$ Resources:WebResources, ManageTiers_CreateNewTier%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span><%--FB 2094--%>
                    </td><td><%--ZD 101244--%>
                    <asp:TextBox ID="txtNewTier1Name" CssClass="altText" runat="server" Text="" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqTier1Name1" ValidationGroup="Create" ControlToValidate="txtNewTier1Name" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                    <%--& <>'+%/\"();?|^&=;!`, fogbugz case 137 for extra junk character validation--%><asp:RegularExpressionValidator ID="regName1" ValidationGroup="Create" ControlToValidate="txtNewTier1Name" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters48%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^@#$%&()~',]*$"></asp:RegularExpressionValidator>  <%--FB 1888--%> <%--ZD 103424--%>
                </td>
            </tr>
            <tr><%--ZD 101244 start--%>
            <td style="width:30%"></td>
            <td class="blackblodtext">
             <asp:Literal Text="<%$ Resources:WebResources, EditEndpoint_lblSecured%>" runat="server"></asp:Literal>
            </td>
            <td> 
              <asp:CheckBox ID="ChkSecure" runat="server" onclick="javascript:EnableValidate();"/>
            </td>
            </tr>    
            <tr>
             <td style="width:30%"></td>
             <td  class="blackblodtext">
              <asp:Literal Text="<%$ Resources:WebResources, ManageTier_securityemail%>" runat="server"></asp:Literal>
             </td>
             <td>
              <asp:TextBox runat="server" ID="txtsecurityemail" CssClass="altText"></asp:TextBox>
              <asp:RequiredFieldValidator ID="reqSecurityemail" Enabled="false" ValidationGroup="Create" ControlToValidate="txtsecurityemail" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
              <asp:CustomValidator id="customsecurityemail" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" Display="Dynamic"
               ControlToValidate="txtsecurityemail"  runat="server"  ClientValidationFunction="fnCheckemail" ValidationGroup="Create" ></asp:CustomValidator>
              <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="Create" ControlToValidate="txtsecurityemail" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters17%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^@#$%&()~]*$"></asp:RegularExpressionValidator>  <%--FB 1888--%>

             <%-- <asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="txtsecurityemail" Display="dynamic" runat="server"  ValidationGroup="Create"
                 ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="regEmail1_2" ControlToValidate="txtsecurityemail" Display="dynamic" runat="server" SetFocusOnError="true"  ValidationGroup="Create"
                ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`,\[\]{}\x22=:#$%&()'~]*$"></asp:RegularExpressionValidator>--%>
             </td>
            </tr>  
            </table></td></tr> 
            <tr></tr>
             <tr id="trTierssubmit" runat="server">
            <td align="center">
             <asp:Button runat="server" ID="btnCreateNewTier1" style="margin-left:-30px" ValidationGroup="Create" Text="<%$ Resources:WebResources, Submit%>" CssClass="altMedium0BlueButtonFormat" OnClick="CreateNewTier1" />
            </td></tr><%--ZD 101244 End--%> 
            </table>
    </div>

<img src="keepalive.asp" alt="keepAlive.aspx" name="myPic" width="0px" height="0px" style="display:none" /> <%--ZD 100419--%> 
    </form>
<%--code added for Soft Edge button--%>
<%--ZD 100420 Start--%>
<script language="javascript">
    if (document.getElementById('txtsecurityemail') != null)
        document.getElementById('txtsecurityemail').setAttribute("onblur", "document.getElementById('btnCreateNewTier1').focus(); document.getElementById('btnCreateNewTier1').setAttribute('onfocus', '');");               
</script>
<%--ZD 100420 End--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

