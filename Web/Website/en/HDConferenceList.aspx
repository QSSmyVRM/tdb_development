<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="HDConferenceList" EnableEventValidation="false"
    Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> <%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8" /><%--FB 2779--%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2050--%>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%--Basic validation function--%>

<script type="text/javascript">

    var servertoday = new Date();
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    //ZD 101028 start
    function fnInvoke(elem) {
        elem.href = "javascript:return void(0)";
        fnOpen('V', elem.name);
        return false;
    }
    //ZD 101028 end
    function fnOpen() {
        var args = fnOpen.arguments;
        var hdnValue = document.getElementById("hdnValue");
        hdnValue.value = args[1];
        if (args[0] == "V") {
            url = "ManageConference.aspx?t=hf&confid=" + args[1];
            confdetail = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
            confdetail.focus();
        }
        else if (args[0] == "D") {
            if (confirm(DeleteConf))
                return true;
            else {
                DataLoading('0');
                hdnValue.value = "";
                return false;
            }
        }
        else if (args[0] == "E") { //ZD 101597
        if (parseInt('<%=Session["admin"]%>') > 0)
            return fnSelectForm();
        else
            return true;
        }
    }

    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Report</title>

    <script type="text/javascript" src="inc/functions.js"></script>

    <style type="text/css">
        <%-- code commented for ZD 101597--%>
        <%--LABEL
        {
            font-size: 8pt;
            vertical-align: top;
            color: black;
            font-family: Arial, Helvetica;
            text-decoration: none;
        }--%>
    </style>
</head>
<body>
    <form id="frmReport" runat="server">
      <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" id="hdnValue" runat="server" />
    <input type="hidden" id="hdnRequestID" runat="server" />
    <input type="hidden" id="hdnEditForm" runat="server" />    <%--ZD 101597--%>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2" align="center" height="10px">
                <h3>
                    <asp:Label ID="lblHeading" runat="server" Text="<%$ Resources:WebResources, HDConfList_HDReservations %>"></asp:Label>
                </h3>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label><br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table width="100%" align="center" border="0">
                    <tr valign="top" runat="server" id="trDetails">
                        <td align="left">
                            <div id="MainDiv" style="overflow-y: auto; overflow-x: auto; word-break: break-all;
                                height: 440px; width: 100%;">
                                <dx:ASPxGridView ID="MainGrid" ClientInstanceName="MainGrid" runat="server" Width="100%"
                                    EnableCallBacks="false" AllowSort="true" OnHtmlRowCreated="MainGrid_HtmlRowCreated"
                                    KeyFieldName="ConferenceID" Styles-Header-Wrap="True" Styles-Cell-HorizontalAlign="Left">
                                    <Settings ShowFilterRow="True" ShowTitlePanel="True" ShowHorizontalScrollBar="true"
                                        ShowVerticalScrollBar="false" ShowHeaderFilterBlankItems="False" />
                                    <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoData%>" GroupPanel="<%$ Resources:WebResources, GridViewGroupMsg%>"/>
                                    <SettingsPager ShowDefaultImages="False" Mode="ShowPager" AlwaysShowPager="true"
                                        Position="Top" Summary-Text="<%$ Resources:WebResources, GridViewPageText%>">
                                    <AllButton Text="<%$ Resources:WebResources, All%>"></AllButton>
                                    <NextPageButton Text="<%$ Resources:WebResources, Next1%>"></NextPageButton>
                                    <PrevPageButton Text="<%$ Resources:WebResources, Prev1%>"></PrevPageButton>
                                    </SettingsPager>
                                </dx:ASPxGridView>
                                <div style="float: right">
                                    <%--FB 2763--%>
				    <br/>
                                    <asp:Button ID="btnCancel" Visible="false" CssClass="altMedium0BlueButtonFormat"
                                        Text="<%$ Resources:WebResources, AudioAddOnBridge_btnGoBack%>" runat="server" OnClick="GoBack" />
                                    <asp:Button style="display:none;" runat="server" ID="Temp" OnClick="lnkEdit_Click" /><%--ZD 101597--%>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <%--ZD 101597--%>
    <div id="PopupFormList" align="center" style="position: fixed; overflow: hidden;
        border: 1px; width: 450px; display: none;  top: 300px;left: 510px;height:450px;">
        <table align="center"class="tableBody" width="80%" cellpadding="5" cellspacing="5">
            <tr class="tableHeader">
                <td colspan="2" align="left" class="blackblodtext">
                    <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ChooseFormHeading%>" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr >
                <td align="left" colspan="2" class="blackblodtext">
                    <asp:RadioButtonList ID="rdEditForm" runat="server"  CssClass="blackblodtext" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="1" CellPadding="3" CellSpacing="3">
                        <asp:ListItem Text="<%$ Resources:WebResources, LongForm%>" Value="1" ></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, ExpressForm%>" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                        <input id="btnOk" runat="server" type="button" class="altMedium0BlueButtonFormat"
                        onclick="javascript:return fnRedirectForm();" value="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" />

                </td>
                <td>
                    <input id="btnDivCancel" runat="server" type="button" class="altMedium0BlueButtonFormat"
                        onclick="javascript:fnDivClose();" value="<%$ Resources:WebResources, Cancel%>" />
                </td>
            </tr>
        </table>
    </div>  
    </form>
</body>
</html>
<script type="text/javascript">
    //<%--ZD 101597 Start--%>

    document.addEventListener("click", handler, true);

    function fnSelectForm() {
        var elementRef = document.getElementById('rdEditForm');
        var inputElementArray = elementRef.getElementsByTagName('input');

        //ZD 102200 Starts
        if ('<%=Session["hasConference"]%>' == '1' && '<%=Session["hasExpConference"]%>' == "1") {
            document.getElementById("PopupFormList").style.display = 'block';
            if (document.getElementById('rdEditForm_1') != null)
                document.getElementById('rdEditForm_1').checked = false;
            if (document.getElementById('rdEditForm_2') != null)
                document.getElementById('rdEditForm_2').checked = false;

            for (var i = 0; i < inputElementArray.length; i++) {
                var inputElement = inputElementArray[i];
                inputElement.checked = false;
            }
            return false;
        }
        //ZD 102200 Ends
    }

    function fnRedirectForm() {
    
        var args = fnRedirectForm.arguments;
        var hdnEditForm = document.getElementById("hdnEditForm");
        hdnEditForm.value = "";

        if (document.getElementById("rdEditForm_0").checked)
            hdnEditForm.value = "1";
        else if (document.getElementById("rdEditForm_1").checked)
            hdnEditForm.value = "2";

        if (hdnEditForm.value != "") {
            var btnTemp = document.getElementById("Temp");
            if (btnTemp != null)
                btnTemp.click();

            DataLoading(1);
            document.getElementById("PopupFormList").style.display = 'None';
        }
        else
            alert("Please select the form to edit a conference.");

        return false;
    }

    function fnDivClose() {
        var hdnEditForm = document.getElementById("hdnEditForm");
        hdnEditForm.value = "";
        document.getElementById("PopupFormList").style.display = 'None';
    }


    function handler(e) {
        var obj = document.getElementById('PopupFormList');

        if (obj.style.display != 'none') {
            if (e.target.id != "Temp" && e.target.id != "btnDivCancel" && e.target.id != "btnOk" && e.target.id.indexOf('rdEditForm') < 0) {
                e.stopPropagation();
                e.preventDefault();
            }
        }

    }

    document.onkeydown = function (event) {
        if (event.keyCode == 27) {
            var obj = document.getElementById('PopupFormList');

            if (obj != null && obj.style.display != 'none')
                obj.style.display = 'none';
        }
    }

    //<%--ZD 101597 End--%>   

</script>
<%--code added for Soft Edge button--%>
<%--<script type="text/javascript">
    var mainDiv = document.getElementById("MainDiv");
    if(mainDiv)
    {  //Difference 180
    
        if (window.screen.width <= 1024)
            mainDiv.style.width = "983px";
        else
            mainDiv.style.width = "1184px";        
    }
    
         
</script>
--%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
